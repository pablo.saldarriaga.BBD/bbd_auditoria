
import os
import sys
import time
import random
import pandas as pd
import datetime as dt
from functools import wraps
from flask import Flask, redirect, request, render_template, url_for, session, flash


#### Para descargar archivos de nltk en AWS elastic beanstalk
import nltk
if os.name !='nt':
    nltk_path = '/tmp/data/nltk_data'
    if not os.path.exists(nltk_path):
        os.makedirs(nltk_path)
        nltk.download('wordnet', download_dir=nltk_path)
    nltk.data.path.append(nltk_path)


def csv_to_df(par_path, par_file, sep):
    par_file = os.path.join(par_path, par_file) 
    df = pd.read_csv(par_file, sep=sep)
    return df

par_path = os.path.dirname(os.path.abspath(__file__))
par_path_data = os.path.join(os.path.join(par_path,"static"), "data")
par_path_code = os.path.join(os.path.join(par_path,"static"), "code")
par_path_info = os.path.join(os.path.join(par_path,"static"), "info")
par_path_sim = os.path.join(os.path.join(par_path,"static"), "simulations")

sys.path.insert(0, par_path_code)
#import generate_economics as recon
#import pd12m_v2 as pds

par_files = os.listdir(par_path_info)
par_files_csv = [files for files in par_files] + ["no_file"]
par_files_economics = [files for files in par_files if ("economics" in files)] + ["no_file"]
par_files_betas = [files for files in par_files if ("MODELO" in files)] + ["no_file"]
par_files_pluto = [files for files in par_files if ("pluto" in files )] + ["no_file"]

par_files_sim = os.listdir(par_path_sim) + ["no_file"]

application = Flask(__name__)



### Para que exija el login
# config
application.secret_key = 'oplpokhmiknfovmmzbxshgjvqhevihekrnekdmelkfmeflemfelfemahdksndekdnecle'

# login required decorator
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('You need to login first.')
            return redirect(url_for('login_form'))
    return wrap



#@application.route('/', methods = ['GET'])
#def my_form():
    #colores = ['final_dataset1', 'final_dataset1']
#    return render_template("index_17.html", colores = par_files_csv)

@application.route('/login')
def login_form():
    return render_template("index.html")

@application.route('/main', methods = ['POST', 'GET'])
def login_full():
    if request.method == 'POST':
       #result = request.form
       usr = request.form['username']
       pwd = request.form['pwd']
       if (usr == 'bigbang' and pwd == 'data') or (usr == 'comfama' and pwd == 'comfamabbd123'):
           session['logged_in'] = True
           return render_template("main.html")
       else:
           return render_template('index.html', message = 'Error en usuario o contraseña')


@application.route('/main_in')
@login_required
def login_full_in():
    return render_template("main.html")


@application.route('/logout')
@login_required
def logout():
    session.pop('logged_in', None)
    session.clear()
    return redirect(url_for('login_form'))


@application.route('/risk_analytics')
@login_required
def risk_analytics():
    return render_template("risk_analysis_dash_board.html")

@application.route('/energy')
@login_required
def energy():
    return render_template("index_energy.html")

@application.route('/text_analytics')
@login_required
#def text_analytics():
#    return render_template("bar_chart_conteos_v1.html")
def text_analytics():

    with open(os.path.join('static','Prueba','key.txt'),encoding='utf-8') as file:
            key=file.read()
    with open(os.path.join('static','Prueba','last_search.txt'),encoding='utf-8') as file:
            search=file.read()
    return render_template("bar_chart_conteos_v1.html",
     Grafica_pib1="./static/Resultados/BarrasConteosCategorias"+key+".html", 
     Grafica_pib="./static/Resultados/SentimientosPorDia"+key+".html",
     Grafica_pib2="./static/Resultados/Conteos_hashtags"+key+".html",
     current_time=int(time.time()),
     search= search)



###############################################################################


### NO usar cache
application.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
application.config['TEMPLATES_AUTO_RELOAD'] = True
@application.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """

    r.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r




@application.route('/')
def login_main():
    return render_template("index.html")
    
#### Facilities location
@application.route('/fac_loc')
@login_required
def fac_loc():
    return render_template("fl2.html")




############### Lo agregado para ChatBot #######

@application.route('/chatbot')
@login_required
def chatbot():
    return render_template("chatbot.html")


##### Lo agregado para dashboards viya
@application.route('/dash_boards')
@login_required
def dash_boards():
    return render_template("text_analytics_dash_board_viya.html")

############### Text mining, tweets table #######

@application.route('/tweets_table')
@login_required
def tweets_table():
    with open(os.path.join('static','Prueba','key.txt'),encoding='utf-8') as file:
        key=file.read()
    with open(os.path.join('static','Prueba','last_search.txt'),encoding='utf-8') as file:
            search=file.read()
    return render_template("tweets_table.html", 
                            csv_file="static/Resultados/TweetsAgrupadosSentimiento"+key+".csv",
                            current_time=int(time.time()),
                            search=search)

############ Descargar tweets table
from flask import send_file
@application.route('/download_tweets')
@login_required
def download_tweets():
    with open(os.path.join('static','Prueba','key.txt'),encoding='utf-8') as file:
            key=file.read()   

    return send_file('static/Resultados/TweetsAgrupadosSentimiento'+key+'.xlsx',
                     mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                     attachment_filename='TweetsAgrupadosSentimiento.xlsx',
                     as_attachment=True, cache_timeout=0)

############### Text mining, tweets map #######
    

@application.route('/tweets_map')
@login_required
def tweets_map():

    with open(os.path.join('static','Prueba','key.txt'),encoding='utf-8') as file:
            key=file.read()
    with open(os.path.join('static','Prueba','last_search.txt'),encoding='utf-8') as file:
            search=file.read()

           
    return render_template("tweets_map.html",
                            url_map='/static/Resultados/MapaTweets'+key+'.html',
                            search= search)


############ Descargar tweets map
@application.route('/download_map')
@login_required
def download_map():
    with open(os.path.join('static','Prueba','key.txt'),encoding='utf-8') as file:
        key=file.read()
    return send_file('static/Resultados/MapaTweets'+key+'.html',
                     attachment_filename='MapaTweets.html',
                     as_attachment=True, cache_timeout=0)
    




############## Descargar graficas tweets ################
    
############ Descargar tweets map
@application.route('/download_dias')
@login_required
def download_dias():
    with open(os.path.join('static','Prueba','key.txt'),encoding='utf-8') as file:
        key=file.read()
    return send_file('static/Resultados/SentimientosPorDia'+key+'.html',
                     attachment_filename='Grafica_dias.html',
                     as_attachment=True, cache_timeout=0)
    
############ Descargar tweets map
@application.route('/download_asociadas')
@login_required
def download_asociadas():
    with open(os.path.join('static','Prueba','key.txt'),encoding='utf-8') as file:
        key=file.read()
    return send_file('static/Resultados/BarrasConteosCategorias'+key+'.html',
                     attachment_filename='Grafica_asociadas.html',
                     as_attachment=True, cache_timeout=0)
    
############ Descargar tweets map
@application.route('/download_hashtags')
@login_required
def download_hashtags():
    with open(os.path.join('static','Prueba','key.txt'),encoding='utf-8') as file:
        key=file.read()
    return send_file('static/Resultados/Conteos_hashtags'+key+'.html',
                     attachment_filename='Grafica_hashtags.html',
                     as_attachment=True, cache_timeout=0)








############### Upload file for replacing sentiment ################
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = os.path.join('static','Resultados')
ALLOWED_EXTENSIONS = {'xlsx','xls'}

application.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
@application.route('/uplofile', methods=['GET', 'POST'])
@login_required
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            path_tweets_acum_exc=os.path.join('static','Resultados','TweetsAgrupadosSentimiento.xlsx')
            filename = secure_filename(file.filename)
            file.save(path_tweets_acum_exc)
            
            ### Reemplazar csv con el nuevo excel
            path_tweets_acum=os.path.join('static','Resultados','TweetsAgrupadosSentimiento.csv')
            exce_fi = pd.read_excel(path_tweets_acum_exc)
            exce_fi = exce_fi[exce_fi.columns[:3]] ### Solo las 3 primeras columnas
            exce_fi.to_csv(path_tweets_acum,index=None)
            
            ### Guardar en excel para quitar celdas calculadas
            if os.path.exists(path_tweets_acum_exc):
                os.remove(path_tweets_acum_exc)
            writer = pd.ExcelWriter(path_tweets_acum_exc, engine='xlsxwriter',options={'strings_to_urls': False, 'strings_to_numbers': False})
            exce_fi.to_excel(writer, index=None)
            writer.close()


            ### Habilitar dropdown lists en excel
            from openpyxl.worksheet.datavalidation import DataValidation
            from openpyxl import load_workbook
            wb = load_workbook(path_tweets_acum_exc)
            ws = wb['Sheet1']
            ws['Z2'] = 'Negativo'
            ws['Z3'] = 'Neutral'
            ws['Z4'] = 'Positivo'    
            current_row = 2
            while current_row < len(exce_fi) + 2:
                ### Dropdown list de 3 opciones de sentimiento
                data_val_status = DataValidation(type="list",formula1='=Z2:Z4')    
                ws.add_data_validation(data_val_status)
                ### Columna C de excel
                row_status = "".join(["C", str(current_row)])    
                data_val_status.add(ws[row_status])   
                current_row += 1
            wb.save(path_tweets_acum_exc)


            ### Esto es temporal, debido a que por ahora unas graficas estan en ingles
            ### y otras en espanol
            dic_ingesp = {}
            dic_ingesp['Negativo'] = 'negative'
            dic_ingesp['Neutral'] = 'neutral'
            dic_ingesp['Positivo'] = 'positive'
            
            
            ### Lectura de los id para el cruce
            exce_faux = pd.read_excel(path_tweets_acum_exc.replace('.xlsx','_aux.xlsx'))
            exce_faux['original_tweet_id'] = exce_faux['original_tweet_id'].astype('str')                
            id_cruc = list(exce_faux['original_tweet_id'].values)
            
            
            #### Cruce para actualizar tabla sentimientos completa (sin agrupar)
            dic_cross = dict(zip(id_cruc,exce_fi.Sentimiento))
            path_full_t = os.path.join('static','Resultados','TweetsRecopiladosSentimiento.xlsx')
            table_comp = pd.read_excel(path_full_t)

            #### Los tweet id sean strings
            table_comp['ID_cruce'] = table_comp['ID_cruce'].astype('str')
                
            sentims = []
            twits_id = table_comp['ID_cruce']
            for t in range(len(table_comp)):
                sentims.append(dic_ingesp[dic_cross[twits_id[t]]])
            table_comp['Sentimiento'] = sentims.copy()

            ### Guardar en excel
            writer = pd.ExcelWriter(path_full_t, engine='xlsxwriter',options={'strings_to_urls': False, 'strings_to_numbers': False})
            table_comp.to_excel(writer, index=None)
            writer.close()



            
            ###### Luego de actualizar el excel, actualizar el resto de archivos
            
            from codigo.preprocessing import clouds_lda_preprocessing
            from codigo.clouds_and_lda import word_clouds
            from codigo.charts import sentiment_day_chart,words_count_chart,hashtags_count_chart
            from codigo.geomap import createmap

            with open(os.path.join('static','Prueba','key.txt'),encoding='utf-8') as file:
                 key=file.read()
            
            path_map=os.path.join('static','Prueba','MapaColombiaGeojson.json')
            path_tweets_sentiment=os.path.join('static','Resultados','TweetsRecopiladosSentimiento.xlsx')
            path_tweets_acum=os.path.join('static','Resultados','TweetsAgrupadosSentimiento'+key+'.csv')

            
            ### Leer tabla de tweets recogidos
            table_recog = pd.read_excel(os.path.join('static','Resultados','TweetsRecogidosApi.xlsx'))            
            
            
            ### Variables a inicializar
            sentiments = sentims.copy()
            date_tweets = list(table_recog['Fecha'].values)
            raw_tweets = list(table_recog['Tweet'].values)
            language = 'es'
            global query
            print(query)
            
            
            #se calculan las fechas unicas
            uniq_dates = pd.unique(pd.Series(date_tweets[:len(sentiments)]).dt.date)
    
            # Se realiza el preprocesamiento para las nubes y LDA
            dataf_sento,processed_tweets,clean_tweets,las_stopwords=clouds_lda_preprocessing(query,raw_tweets,language,
                                                                                            sentiments,date_tweets)
    
            # Se genera el mapa
            createmap(path_tweets_sentiment, path_map,key)
            send_file('static/Resultados/MapaTweets'+key+'.html', cache_timeout=0)
    
    
    
            ### Se borran las wordcloud anteriores
            results_folder = 'static/Resultados'
            if os.path.exists(results_folder):
                for fi in os.listdir(results_folder):
                    if '.'  in  fi[1:]:  ## No borrar folders
                        if 'NubeTerminosTweets' in fi:  ### Borrar solo lo relacionado con nubes
                            os.remove(os.path.join(results_folder,fi))             
            
            
    
            #Se generan las wordclouds
            word_clouds(query,dataf_sento,uniq_dates,las_stopwords,language)
    
            #Se genera el gráfico de sentimiento por día
            sentiment_day_chart(query,dataf_sento,key)
    
            # se crean las gráficas de conteo de palabras 
            df_graph=words_count_chart(raw_tweets,sentiments,key)
            
            ## se crean las gráficas de conteo de hashtags 
            df_graph,tweets_hashtags=hashtags_count_chart(raw_tweets,sentiments,key)

            return redirect(url_for('tweets_table',
                                    filename=filename))
    else:
        return render_template('load_file.html')
    # return '''
    # <!doctype html>
    # <title>Suba el archivo descargado de excel con los sentimientos editados</title>
    # <h1>Suba el archivo descargado de excel con los sentimientos editados</h1>
    # Debe ser el mismo archivo que se descargó usando el botón descargar,
    # solo se le deben editar los sentimientos usando la lista desplegable.
    
    # <br />
    # <br />
    # <form method=post enctype=multipart/form-data>
    #   <input type=file name=file>
    #   <input type=submit value=Confirmar>
    # </form>
    # '''
#########








################ Prueba Bokeh server
#from bokeh.embed import server_document
#
##### Run bokeh servers in the background
#ruta_python = sys.executable
#print('parpa')
#print(str(par_path))
#import subprocess
#linea = ruta_python + ' -m bokeh serve ' +  par_path + '/static/Prueba/bokeh-sliders.py' + ' --allow-websocket-origin=localhost:5000'
#print('linea')
#print(linea)
#print('ospathjoin')
#print(str(os.path.join(par_path, '/static/Prueba/bokeh-sliders.py ')))
#cal = subprocess.Popen(linea, shell=True)
#
##### Run bokeh servers in the background
#print('parpa')
#print(str(par_path))
#
#
#@application.route("/ejemplo_back")
#@login_required
#def hello():
#    # script=server_document(model=None,app_path="/bokeh-sliders",url="http://localhost:5006")
#    script=server_document("http://localhost:5006/bokeh-sliders")
#    print(script)
#    return render_template('hello.html',bokS=script)
#












###############################################################################





@application.route('/form_sim', methods = ['GET', 'POST'])
@login_required
def form_sim():
    files_sim = os.listdir(par_path_sim)
    par_files_sim = [files for files in files_sim if ("par_" not in files)]
    par_files_par = [files for files in files_sim if ("par_" in files)]
    df_simulations = pd.DataFrame(par_files_par, columns = ["simulation_name"])
    df = pd.DataFrame(columns = ["time_execution","economics_file", "pluto_file", "betas_file"])
    for files in par_files_par:
        df_data = csv_to_df(par_path_sim, files, ',')
        par_time = df_data[df_data["parameter"] == "time"]["value"].values[0]
        par_economics = df_data[df_data["parameter"] == "economics_file"]["value"].values[0]
        par_pluto = df_data[df_data["parameter"] == "pluto_file"]["value"].values[0]
        par_betas = df_data[df_data["parameter"] == "betas_file"]["value"].values[0]
        df_par = pd.DataFrame(data = [[par_time,
                                    par_economics,
                                   par_pluto,
                                   par_betas]],
                          columns = ["time_execution",
                                    "economics_file",
                                     "pluto_file",
                                     "betas_file"])
        df = df.append(df_par, ignore_index = True)
    df_simulations = pd.concat([df_simulations, df], axis = 1)
    return render_template("form_sim_v3.html",
                           csv_general = par_files_csv,
                           csv_economics = par_files_economics,
                           csv_betas = par_files_betas,
                           csv_pluto = par_files_pluto,
                           filiales = ["AGR", "BCO", "no_filial"],
                           options = ["Yes", "No"],
                           csv_sim = par_files_sim + ["no_file"],
                           tables = [df_simulations.to_html(classes = 'female')],
                           titles = ['na', 'Registered Simulations'])

@application.route('/result',methods = ['GET','POST'])
@login_required
def tweets():
    import datetime as dt
    import pandas as pd
    import os
    if request.method =='POST':
        print(str(request.form))
        tema = request.form['tema']
        numero = request.form['numero']
        palabras=[]
        if request.form['palabra1']!="":
            palabras.append(request.form['palabra1'])
        if request.form['palabra2']!="":
            palabras.append(request.form['palabra2'])
        if request.form['palabra3']!="":
            palabras.append(request.form['palabra3'])
        
        ### Si no escribi ninguna palabra, para que no sque error, use la del query
        if palabras==[]:
            for pa in tema.split(' '): 
                if pa.lower()!='or':
                    paa = pa.lower().replace('(', '').replace(')', '').strip()
                    palabras.append(paa)        


                
        global query
        quer = tema
        
        try:
            ### Si saca error, hay un problema con el input de fecha
            

            d_ini = request.form['d_ini']
            d_ini = pd.to_datetime(d_ini, format = "%Y-%m-%d").strftime("%Y-%m-%d")
            
            d_fin = request.form['d_fin']
            d_fin = pd.to_datetime(d_fin, format = "%Y-%m-%d") + dt.timedelta(days = 1)
            d_fin = d_fin.strftime("%Y-%m-%d")           
  
            
        except Exception as e:
            print(f'Error con el formato de fecha ingresada: {e}')
            d_ini = dt.datetime.now().strftime("%Y-%m-%d")
            d_fin = dt.datetime.now()  + dt.timedelta(days = 1)
            d_fin = d_fin.strftime("%Y-%m-%d")
        
        key=str(dt.datetime.now())
        key=key.replace('.','')
        key=key.replace(':','')

        with open(os.path.join('static','Prueba','key.txt'),'w',encoding='utf-8') as file:
            file.write(key)

        with open(os.path.join('static','Prueba','last_search.txt'),'w',encoding='utf-8') as file:
            file.write(tema)

        print(d_ini)
        print(d_fin)

        if request.form.__contains__('cbox_num'):
            search_num = request.form['cbox_num'] == 'on'
        else:
            search_num = False


        if request.form.__contains__('cbox_date'):
            search_date = request.form['cbox_date'] == 'on'
        else:
            search_date = False

        


        ### Numero de tweets que se quieren analizar (los que se miran)
        ### Si se decide no incluir repetidos, se miran numero_tweets, pero muchos de ellos
        ### se descartarian y probablemente nos quedemos con un numero de twets menor a ese
        try:
            numero_tweets = int(numero)
        except ValueError:
            numero_tweets = 0


        #numero_tweets = int(numero)
        
        #### Voy a limitarlo por ahora a maximo 500 tweets (para que no haya
        ### un error de llenado de formulario y nos cobre un valor grandisimo en AWS comprehend)
        numero_tweets = min(500, numero_tweets)


        ### Para que genere resultados bonitos, minimo que tenga 30 tweets
        numero_tweets = max(30, numero_tweets)        

 
        #######################################################################
        ################ Verificar si aun hay cupo diario para procesar mas tweets
          
        ### Maximos diarios
        maximos_diarios = 5000
        
        ### Log con los tweets procesados diarios
        cupos_diarios = pd.read_csv('static/logs/ProcesadosPorDia.csv')
        
        #### Tomo los dias existentes
        dias_exis = list(cupos_diarios['Fecha'])
        conteos_exis = list(cupos_diarios['Procesados'])
        
        ### Verifico si el dia de hoy existe en los dias existentes
 

        now = dt.datetime.now()
        fecha_hoy = str(now.year)+'-'+str(now.month)+'-'+str(now.day)
        if fecha_hoy in dias_exis:
            
            ### Si si existe, verificar cuantos ya habia procesado hoy
            procesados_hoy = cupos_diarios[cupos_diarios['Fecha']==fecha_hoy]['Procesados'].values[0]
        
            
            ### Si el limite de procesados diarios ya fue superado, terminar aqui la ejecucion
            if procesados_hoy > maximos_diarios:
                return render_template('limite_superado.html')
            
            else:
                ### Si el limite aun no habia sido superado, agregar los tweets procesados
                ### en esta ejecucion
                conteos_exis[dias_exis.index(fecha_hoy)] = procesados_hoy + numero_tweets
                cupos_diarios['Procesados'] = conteos_exis.copy()    
            
        else:
            ### si no existia, agregar esta fila de procesados hoy
            cupos_diarios = cupos_diarios.append(pd.Series({'Fecha':fecha_hoy, 'Procesados':numero_tweets}), ignore_index=True)
                
        
        ###### Guardo el csv con el numero de procesados actualizado
        cupos_diarios.to_csv('static/logs/ProcesadosPorDia.csv', index=None)            
        
        ##### Si aun se tiene cupo diario, puedo seguir con los demas pasos
        #######################################################################
        
        ### Limpio los archivos de resultado de anteriores ejecuciones
        results_folder = 'static/Resultados'
        if os.path.exists(results_folder):
            for fi in os.listdir(results_folder):
                if '.'  in  fi[1:]:  ## No borrar folders
                    os.remove(os.path.join(results_folder,fi))  
        
        
        ### Descargo nltk data (solo para el linux de aws) 
        if os.name !='nt':
            nltk_path = '/tmp/data/nltk_data'
            if not os.path.exists(nltk_path):
                os.makedirs(nltk_path)
                nltk.download('wordnet', download_dir=nltk_path)
            nltk.data.path.append(nltk_path)     


        pd.DataFrame(palabras,columns=['Categoría']).to_excel(os.path.join('static','Prueba','InfoUsuario.xlsx'), index=False)

        #Parametros
        
        pd.DataFrame(palabras, columns=['Categoría']).to_excel(os.path.join('static','Prueba','InfoUsuario.xlsx'), index=False)

        # Variables para la busqueda
        date_ini=d_ini
        date_fin=d_fin
        query=quer
        language='es'
        number_of_tweets=numero_tweets
        utc=-5
        repeated_tweets = True
        tweets_per_query=100
        
        ### Para busqueda avanzada
        query = query.lower().replace(' or ',' OR ').strip()  


        
        #paths
        path_folder_r = os.path.join('static','Resultados')
        path_cities=os.path.join('static','Prueba','ciudades y departamentos.xlsx')
        path_tweets=os.path.join('static','Resultados','TweetsRecogidosApi.xlsx')
        path_synonyms=os.path.join('static','Prueba','SinonimosEnSentimiento.xlsx')
        path_words_list=os.path.join('static','Prueba','ListadoPalabras.txt')
        path_map=os.path.join('static','Prueba','MapaColombiaGeojson.json')
        path_stopwords=os.path.join('static','Prueba','StopwordsSentiment.txt')
        path_tweets_sentiment=os.path.join('static','Resultados','TweetsRecopiladosSentimiento.xlsx')
        path_tweets_acum=os.path.join('static','Resultados','TweetsAgrupadosSentimiento'+key+'.csv')


        if not os.path.exists(path_folder_r):
            os.makedirs(path_folder_r)




        import logging
        import pdb
        import pandas as pd
        import datetime
        #from parametros import *
        from codigo.twitter import extract_tweets
        from codigo.twitter_premium import extract_tweets as extract_tweets2
        from codigo.save_to_files import tweets_to_file,tweets_sentiment_to_file,acum_tweets_to_file
        from codigo.preprocessing import sentiment_analysis_preprocessing,clouds_lda_preprocessing
        from codigo.amazon import sentiment_analysis
        from codigo.calculates import retweet_count
        from codigo.clouds_and_lda import word_clouds,generate_lda,tweets_similarity,lda_graph
        from codigo.charts import sentiment_day_chart,words_count_chart,hashtags_count_chart
        from codigo.geomap import createmap

        logging.basicConfig(format="%(asctime)s:%(levelname)s:%(message)s",level=logging.INFO)     

        logging.info('Ejecutando demo de text mining...\n')

        
        diff_dates = pd.to_datetime(dt.datetime.now()) - pd.to_datetime(date_ini)
        
        if diff_dates.days <= 7:
            # se utiliza la API de twitter y se busca información seleccionada
            raw_tweets,original_tweet_id,date_tweets,location_tweets, tweet_id=extract_tweets(query,number_of_tweets,tweets_per_query,language,
                                                                                            date_ini,date_fin,repeated_tweets,utc,
                                                                                            search_num,search_date)
        else:
            # se utiliza el api premium
            raw_tweets,original_tweet_id,date_tweets,location_tweets, tweet_id=extract_tweets2(query,number_of_tweets,tweets_per_query,language,
                                                                                            date_ini,date_fin,repeated_tweets,utc,
                                                                                            search_num,search_date)






        ############ Guardar registro de busquedas
        ### Solo guardarlos si estoy en el linux de aws
        if os.name !='nt':
            ### Leer registros anteriores de AWS S3
            import boto3
            s3 = boto3.client(service_name='s3', region_name='us-east-1')
            s3.download_file('paginawebtextmining', 'logs/RegistroBusquedas.csv', 'tempofilee.csv')
            data_regis_d = pd.read_csv('tempofilee.csv')
            ### Crear registro de esta busqueda
            data_regis_d = data_regis_d.append(pd.Series(), ignore_index=True)
            data_regis_d['Fecha_consulta'][len(data_regis_d)-1] = datetime.datetime.now() - datetime.timedelta(hours=5)
            data_regis_d['Query'][len(data_regis_d)-1] = query
            data_regis_d['Numero_tweets'][len(data_regis_d)-1] = str(int(len(raw_tweets)))
            data_regis_d['date_ini'][len(data_regis_d)-1] = date_ini
            data_regis_d['date_fin'][len(data_regis_d)-1] = date_fin
            
            ### Subir archivo actualizado a AWS S3
            data_regis_d.to_csv('tempofilee.csv', index=None)
            s3.upload_file('tempofilee.csv', 'paginawebtextmining', 'logs/RegistroBusquedas.csv')
        
                
            
        # si no hay tweets, redireccionar
        if(len(raw_tweets)< 1):
            # return new template
            if len(location_tweets) >= 1:
                return redirect(url_for('no_results', warning=True))
            else:
                return redirect(url_for('no_results', warning=False))

        else:
            logging.info('r_tweet num' + str(len(raw_tweets)))
            logging.info('d_tweet num' + str(len(date_tweets)))
            logging.info('l_tweet num' + str(len(location_tweets)))









        # se guarda la información de los tweets en archivos de excel
        tweets_to_file(location_tweets,raw_tweets,original_tweet_id,date_tweets,path_cities,path_tweets, tweet_id)

        #Se realiza todo el preprocesamiento para el análisis de sentimiento
        rebuild_tweets,twe_rem=sentiment_analysis_preprocessing(raw_tweets,path_synonyms,path_words_list,path_stopwords)

        #Se realiza el análisis de sentimiento con amazon comprehend
        sentiments=sentiment_analysis(rebuild_tweets,original_tweet_id,twe_rem)

        #se calculan las fechas unicas
        uniq_dates = pd.unique(pd.Series(date_tweets[:len(sentiments)]).dt.date)

        # Se realiza el preprocesamiento para las nubes y LDA
        dataf_sento,processed_tweets,clean_tweets,las_stopwords=clouds_lda_preprocessing(query,raw_tweets,language,
                                                                                        sentiments,date_tweets)

        #Se guardan los tweets con el sentimiento en excel
        tweets_recop=tweets_sentiment_to_file(dataf_sento,raw_tweets,date_tweets,sentiments,path_tweets,path_tweets_sentiment, original_tweet_id, tweet_id)

        #Se realiza un conteo de los retweets
        tweets_acum_g=retweet_count(tweets_recop,original_tweet_id, tweet_id)

        #Se guarda el archivo
        acum_tweets_to_file(tweets_acum_g,path_tweets_acum)

        # Se genera el mapa
        createmap(path_tweets_sentiment, path_map,key)
        send_file('static/Resultados/MapaTweets'+key+'.html', cache_timeout=0)

        #Se generan las wordclouds
        word_clouds(query,dataf_sento,uniq_dates,las_stopwords,language)

        #Se genera el gráfico de sentimiento por día
        sentiment_day_chart(query,dataf_sento,key)

        #Se genera el modelo LDA
        #lda_model,bow_corpus,dictionary=generate_lda(processed_tweets)

        #se calcula la matriz de similaridades
        #tweets_similarity(query,raw_tweets,dataf_sento,uniq_dates,bow_corpus,clean_tweets)

        # se crean las gráficas de conteo de palabras 
        df_graph=words_count_chart(raw_tweets,sentiments,key)


        ## se crean las gráficas de conteo de hashtags 
        df_graph,tweets_hashtags=hashtags_count_chart(raw_tweets,sentiments,key)

        # se crea gráfica de LDA
        #lda_graph(lda_model,bow_corpus,dictionary)

        logging.info('\nFin del programa.\n')

        return redirect('/tweets_table')
  

@application.route('/configurat', methods = ['GET'])
@login_required
def configurat():
    #colores = ['final_dataset1', 'final_dataset1']
    return render_template("index_17.html", colores = par_files_csv)

@application.route('/no_results', methods = ['GET'])
@login_required
def no_results():
    wrng = request.args['warning'] == 'True'
    print(wrng)
    return render_template('no_result.html', warning = wrng)

@application.route('/visual', methods = ['GET', 'POST'])
@login_required
def visual():
    if request.method == 'POST':
        file_name = request.form.getlist('show_visual')
        print(file_name[0])
        path_file_name = os.path.join(par_path_info, file_name[0])
        data = pd.read_excel(path_file_name)
        tables = []
        titles = ['na']
        if "MODELO" in file_name[0]:
            for i in data.T.columns:
                tables.append(pd.DataFrame(data.T[i].dropna()).T.drop(columns = ["NOMBRE"]).to_html(classes='female'))
                titles.append(pd.DataFrame(data.T[i].dropna()).T["NOMBRE"].values[0])
            return render_template('tables_1_v1.html', tables = tables,#, males.to_html(classes='male')],
            titles = titles)#, 'Male surfers'])
        else:
            return render_template('tables_1_v1.html', tables = [data.to_html(classes = 'female')], titles = ['na', file_name[0]])


@application.route('/visual_graph', methods = ['GET', 'POST'])
@login_required
def visual_graph():
    if request.method == 'POST':
        file_name = request.form.getlist('graph')
        sheet_name = file_name[0][0:-5]
        file_name_generated = "economics_generated_" + str(random.random()) + ".csv"
        recon.generate_economics_file(par_path_info,
                                      file_name[0],
                                      sheet_name,
                                      par_path_data,
                                      file_name_generated)
        #return render_template("index_economics.html", variable_name = file_name_generated,
        #                                               variable_title = sheet_name)
        return render_template("visualize_new_v1.html", variable_name = file_name_generated,
                                                     variable_title = sheet_name)

@application.route('/word_cloud_dash_v1', methods = ['GET', 'POST'])
@login_required
def word_cloud_dash_v1():
    import json
    par_path = os.path.dirname(os.path.abspath(__file__))
    par_path_prueba = os.path.join(os.path.join(par_path,"static"))
    files_result=os.listdir(os.path.join(par_path_prueba,'Resultados'))
    par_files_cloud=[]
    sentiment=['Totales','Positivos','Neutrales','Negativos']
    with open(os.path.join('static','Prueba','last_search.txt'),encoding='utf-8') as file:
            search=file.read()
    for file in files_result:
        if 'ConteoNubeTerminosTweets' in file and file[-14:-4] not in par_files_cloud:
            
            # ### solo incluir si si genero las 3 nubes
            # if 'ConteoNubeTerminosTweetsPositivos'+file[-14:-4]+'.txt' in files_result:
            #     if 'ConteoNubeTerminosTweetsNegativos'+file[-14:-4]+'.txt'  in files_result:
            #         if 'ConteoNubeTerminosTweetsNeutrales'+file[-14:-4]+'.txt'  in files_result:
                        try:
                            dt.datetime.strptime(file[-14:-4],'%Y-%m-%d')
                            par_files_cloud.append(file[-14:-4])
                        except Exception:
                            pass

    par_files_cloud.sort()
    par_files_cloud.insert(0,'Total')

    if request.method == 'GET':
        par_cloud_file = par_files_cloud[0]
        sentiment_selected = sentiment[0]
        try:
            if par_cloud_file =='Total':
                
                with open(os.path.join(par_path_prueba,'Resultados/ConteoNubeTerminosTweets'+sentiment_selected+'.txt'),encoding='utf-8') as file:
                    file_selected=file.read()

            else:
                
                with open(os.path.join(par_path_prueba,'Resultados/ConteoNubeTerminosTweets'+sentiment_selected+par_cloud_file+'.txt'),encoding='utf-8') as file:
                    file_selected=file.read()
        except Exception:
           flash('No hay tweets disponibles para la selección realizada')
           file_selected='' 
        print(par_files_cloud)
        print (file_selected)
        print(par_cloud_file)

    if request.method == 'POST':
        par_cloud_file = request.form.getlist('cloud_file')[0]
        sentiment_selected=request.form.getlist('sentiment_selected_form')[0]
        try:
            if par_cloud_file =='Total':      
                
                with open(os.path.join(par_path_prueba,'Resultados/ConteoNubeTerminosTweets'+sentiment_selected+'.txt'),encoding='utf-8') as file:
                    file_selected=file.read()
            else:
                
                with open(os.path.join(par_path_prueba,'Resultados/ConteoNubeTerminosTweets'+sentiment_selected+par_cloud_file+'.txt'),encoding='utf-8') as file:
                    file_selected=file.read()
        except Exception:
            flash('No hay tweets disponibles para la selección realizada')
            file_selected=''

    print(par_files_cloud)
    print (file_selected)
    print(par_cloud_file)

    return render_template("word_cloud_v6.html",  variable_name = par_cloud_file,
                                                    word_cloud_files = par_files_cloud,
                                                    condicion = par_cloud_file,
                                                    sentiment=sentiment,
                                                    sentiment_selected=sentiment_selected,
                                                    text_selected=json.dumps(file_selected),
                                                    search = search)
@application.route('/textmining')
@login_required
def conteos():
    return render_template("text_mining_menu_v1.html")

@application.route('/load_PQRs', methods = ['GET', 'POST'])
@login_required
def load_pqrs():
    if request.method == 'GET':
        return render_template('load_pqrs.html')
    if request.method == 'POST':
        return redirect('/textmining')

@application.route('/graph', methods=['GET','POST'])
@login_required
def graph():
    
    #### Encontrar html de graphs
    ruta_graphs = 'static/Resultados'
    files_graphs = os.listdir(ruta_graphs)
    par_files_graph_aux = list(filter(lambda f: f.startswith('Grafo_similitudes_') ,files_graphs))
    par_files_graph = []
    for i in par_files_graph_aux:
        par_files_graph.append(i.replace('_similitudes_tweets',' ')[:-5])
    
    ### Leer contenido de grafo base
    grafo_base = open('templates/grafo_base.html', encoding='utf-8')
    grafo_base_t = grafo_base.read()
    grafo_base.close()
    
    #### Pasar los archivos de grafos a la carpeta de templates
    ruta_destin = 'templates'
    grafos_names = []
    count_name = 0
    for o in par_files_graph_aux:
        name_destiny = 'grafo_'+str(count_name)+'.html'
        
        ### Leer contenido de grafo actual
        grafo_actual = open(os.path.join(ruta_graphs, o), encoding='utf-8')
        grafo_actual_c = grafo_actual.read()
        grafo_actual.close()
        
        #### Extraigo del html del grafo actual la parte importante
        inici = grafo_actual_c.find('<div class="bk-root"')    
        mid = grafo_actual_c[inici:].find('</script>')
        ende = grafo_actual_c[inici+mid+9:].find('</script>')
        a_reemplazar = grafo_actual_c[inici:inici+mid+ende+18]
        
        ####  Reemplazo en el html de grafo base
        grafo_html = grafo_base_t.replace('REEMPLAZAR_AQUI', a_reemplazar)
        
        ### Guardo el html
        grafo_guardar = open(os.path.join(ruta_destin, name_destiny), 'w', encoding='utf-8', )
        #tempora = grafo_guardar.write(grafo_html)
        grafo_guardar.close()
    
        grafos_names.append(name_destiny)
        count_name = count_name+1
    
    
#    par_files_graph = [ 'Grafo 2019-05-22',
#                    'Grafo 2019-05-23',
#                    'Grafo 2019-05-24',
#                    'Grafo 2019-05-25',
#                    'Grafo 2019-05-26',
#                    'Grafo 2019-05-27',
#                    'Grafo 2019-05-28',
#                    'Grafo 2019-05-29']
    if request.method == 'GET':
        return render_template(grafos_names[0], graph_files = par_files_graph)

    if request.method == 'POST':
        #result = request.form
        par_graph_file = request.form.getlist('graph_file')[0]
        par_graph_which = par_files_graph.index(par_graph_file)
        return render_template(grafos_names[par_graph_which], graph_files = par_files_graph, condicion = par_files_graph[par_graph_which])


@application.route('/heatmap', methods = ['GET', 'POST'])
@login_required
def heatmap():
    par_files_graph = [ 'Grafo 2019-05-22',
                    'Grafo 2019-05-23',
                    'Grafo 2019-05-24',
                    'Grafo 2019-05-25',
                    'Grafo 2019-05-26',
                    'Grafo 2019-05-27',
                    'Grafo 2019-05-28',
                    'Grafo 2019-05-29']
    if request.method == 'GET':
        return render_template("heatmap_v3.html", graph_files = par_files_graph,
                                                  condicion = 'Grafo 2019-05-22',
                                                  source_file = '2019-05-22' + 'matrix_data.csv')
    if request.method == 'POST':
        #result = request.form
        par_graph_file = request.form.getlist('matrix_file')[0]
        print(par_graph_file[6:])
        return render_template("heatmap_v3.html",
                               graph_files = par_files_graph,
                               condicion = par_graph_file,
                               source_file = par_graph_file[6:] + "matrix_data.csv")

@application.route('/bar')
@login_required
def bar():
    return render_template("bar_chart_last.html")

@application.route('/visualize_new')
@login_required
def visualize_new():
    return render_template("map_1.html")

@application.route('/dashboard')
@login_required
def dashboard():
    return render_template("form_sim_v3.html")

@application.route('/auxiliar_ldavis')
@login_required
def js():
    return render_template('ldavis.js')
    
@application.route('/visualizacion_tematicas')
@login_required
def grafica_Lda():
    return render_template('LDA.html')



if __name__ == '__main__':
    ############### SIEMPRE DEJAR DEBUG EN FALSE, PARA EVITAR ATAQUES #########
    application.run(debug=False)
