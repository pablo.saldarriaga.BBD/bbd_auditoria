******************************************************************;
*   TRAZABILIDAD DE CAMPAÑAS                                     *;
*
* Descripcion: Importación y cruce de reportes de terra y lirela *;
******************************************************************;

options symbolgen;
%global file_nm_var;
%include "/sasdata/app/config/macros/import_terra.sas";
%include "/sasdata/app/config/macros/import_lirela.sas";
%include "/sasdata/app/config/autoexec.sas";



/*1)Importación de reporte lirela*/

/*listado de archivos contenidos en carpeta lirela*/

data listado_archivos(keep=file_name);
	rc = filename('xdir', "&path_lirela.");

	if rc eq 0 then
		do;
			dirID = dopen('xdir');

			if dirID ne 0 then
				do;
					nFiles = dnum(dirID);

					do i = 1 to nFiles;
						file_name = dread(dirID, i);
						output;
					end;
				end;
		end;
run;

/*4).Crear variable file_nm_var que contiene los nombres de los archivos a importar*/
proc sql;
	select distinct
		file_name
	into :file_nm_var separated by ' '
		from listado_archivos;
quit;

/*%let file_nm_var = &file_nm_var.;*/

/*5) Eliminar tabla reporte_terra*/
data _null_;
 if exist ("work.reporte_lirela") then 
 call execute("proc datasets library=work; delete reporte_lirela;run;quit;") ;
run;

/*6) Ejecutar macro para la importación de archivos de Terra*/
%import_lirela(&file_nm_var.);


/*3)Importación Reporte Terra*/

/*listado de archivos contenidos en carpeta terra*/

data listado_archivos(keep=file_name);
	rc = filename('xdir', "&path_terra.");

	if rc eq 0 then
		do;
			dirID = dopen('xdir');

			if dirID ne 0 then
				do;
					nFiles = dnum(dirID);

					do i = 1 to nFiles;
						file_name = dread(dirID, i);
						output;
					end;
				end;
		end;
run;

/*4).Crear variable file_nm_var que contiene los nombres de los archivos a importar*/
proc sql;
	select distinct
		file_name
	into :file_nm_var separated by ' '
		from listado_archivos;
quit;

/*%let file_nm_var = &file_nm_var.;*/

/*5) Eliminar tabla reporte_terra*/
proc sql;
	drop table reporte_terra;
quit;

/*6) Ejecutar macro para la importación de archivos de Terra*/
%import(&file_nm_var.);

data datamart.reporte_terra;
  set work.reporte_terra;
run;

/*7)Cruce de reporte_contactabilidad,reporte_lirela, reporte_terra*/
proc sql;
	connect to oracle (user="DATAMART" password="{SAS002}3A30C8281E52F121102B599725B76D72257CC433" PATH="sasdb.otecel.com.ec");
    
   execute (drop table  datamart.TMP1 )by oracle;
			execute (drop table  datamart.rtdm_trazabilidad_campanas )by oracle;
   execute (create table datamart.TMP1 as 
												select a.*, b.LIRELA,b.hora_envio
												from datamart.reporte_contactabilidad_pr a left join
												datamart.reporte_lirela b on a.tracking_esp=b.tracking_esp and a.keyword=b.keyword) by oracle;
    execute (create table datamart.rtdm_trazabilidad_campanas as 
														select a.*, b.TERRA
														from datamart.TMP1 a
														left join datamart.reporte_terra b on a.tracking_esp=b.tracking_esp) by oracle;
		 	/*execute (update datamart.rtdm_trazabilidad_campanas set Terra=0
												where Terra is null) by oracle;
			 execute (update  datamart.rtdm_trazabilidad_campanas set LIRELA=0
												where LIRELA is null) by oracle;*/
    disconnect from oracle;
quit;

data reporte_lirela_3;

set datamart.reporte_lirela(rename=(hora_envio=hora_enviochar));

hora_envio = input(hora_enviochar, anydtdtm.);
format hora_envio datetime25.;

drop hora_enviochar;

run;


