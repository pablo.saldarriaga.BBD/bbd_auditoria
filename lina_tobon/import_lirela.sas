﻿/*4.Crear y ejecutar macro soble la vaiable file_nm_var, para crear  archivo sms_export_1 que
tiene el contenido de los archivos de exportacion de las campa�as de interes*/

%macro import_lirela(files);
%local i nextArchivo ;
	%let howManyFiles = %sysfunc(countw(&files., ' '));

	%do i = 1 %to &howManyFiles.;
		%let nextArchivo = %scan(&files., &i., ' ');
		%put &=nextArchivo;

			data file_&i.;
   infile "&path_lirela./&nextArchivo" delimiter = ';' FLOWOVER DSD firstobs=2;
   
      informat SHORTCODE $3. ;
      informat FECHA $20. ;
      informat FECHA_HORA $20. ;
						informat TELEFONO $20. ;
      informat MENSAJE_RECIBIDO $20. ;
      informat MENSAJE_ENVIADO $200. ;
      informat ERROR $200. ;
      informat HORA_ENVIO $20.;
      
  input
       SHORTCODE  $
       FECHA  
       FECHA_HORA
							TELEFONO $ 
       MENSAJE_RECIBIDO
       MENSAJE_ENVIADO  $
       ERROR  $
       HORA_ENVIO $;   
run;

proc append base=reporte_lirela data=file_&i. force nowarn;
run;

/*2) Preparación de datos*/

 data reporte_lirela;
	set reporte_lirela;
	where error is null;
		i=find(mensaje_recibido,"_");
 	LIRELA=1;

 	if i>0  then do;
			keyword=substr(mensaje_recibido,1,i-1); /*obtener numeros antes del guion */
			tracking_esp=substr(mensaje_recibido,i+1); /*obtener numeros despues del guion */

			output reporte_lirela;

		end;

	drop i;
run;

	%end;

data datamart.reporte_lirela;

set reporte_lirela(rename=(hora_envio=hora_enviochar));

hora_envio = input(hora_enviochar, anydtdtm.);
format hora_envio datetime25.;

drop hora_enviochar;

run;
%mend;
